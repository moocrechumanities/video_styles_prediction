import os
import sys
from transform import transform
from predict.predict import predict
import shutil


def all_subdirs_of(b='.'):
    result = []
    for d in os.listdir(b):
        bd = os.path.join(b, d)
        if os.path.isdir(bd):
            result.append(bd)
    return result


def getAverage():
    all_subdirs = (all_subdirs_of('../course'))

    latest_subdir = max(all_subdirs, key=os.path.getmtime)

    print(latest_subdir)

    transform(latest_subdir, latest_subdir.split('/')[-1])

    predict_location = os.getcwd() + '/frames/' + \
        latest_subdir.split('/')[-1]
    predictions = predict(predict_location)

    conversation = 0
    person = 0
    text = 0
    print(len(predictions))
    total = len(predictions)
    for prediction in predictions:
        print(prediction)
        if prediction["style"] == "conversation":
            conversation += 1
        elif prediction["style"] == "person":
            person += 1
        elif prediction["style"] == "text":
            text += 1

    print("conv", (conversation / total) * 100)
    print("person", (person / total) * 100)
    print("text", (text / total) * 100)

    convAverage = (conversation / total) * 100
    personAverage = (person / total) * 100
    textAverage = (text / total) * 100

    shutil.rmtree('./frames/')
    os.mkdir('./frames')

    return {
        'conversation': convAverage,
        'person': personAverage,
        'text': textAverage
    }
