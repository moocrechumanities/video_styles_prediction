import cv2
import math
import os
#import matplotlib.pyplot as plt
# %matplotlib inline
#from keras.preprocessing import image
#import numpy as np
#from keras.utils import np_utils
#from skimage.transform import resize


def transform(folderPath, folderName):
    path = 'frames/' + folderName
    os.mkdir(path)
    dirCount = 0
    for dirpath, dirs, files in os.walk(folderPath + '/'):
        dirCount += 1
        fileCount = 0
        for file in files:
            print(file)
            fileCount += 1
            if('.mp4' in file):
                count = 0
                videoFile = dirpath + '/' + file
                cap = cv2.VideoCapture(videoFile)
                frameRate = cap.get(5)
                while(cap.isOpened()):
                    frameId = cap.get(1)
                    ret, frame = cap.read()
                    if(ret != True):
                        break
                    if(frameId % (math.floor(frameRate) * 2) == 0):
                        filename = "%d-%d" % (dirCount, fileCount)
                        filename += "frame%d.jpg" % count
                        print("Converting frame : ", filename)
                        count += 1
                        cv2.imwrite(os.path.join(
                            path, filename), frame)
                cap.release()
    print("Done")
