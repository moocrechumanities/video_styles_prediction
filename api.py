from flask import Flask
from main import getAverage

app = Flask(__name__)


@app.route("/")
def hello():
    average = getAverage()
    return average

app.run(host='0.0.0.0',port=5000)
